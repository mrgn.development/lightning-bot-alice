#!/bin/bash

sudo systemctl stop lightningbot.service
cd /opt/bot/
svn up
npm install
sudo systemctl start lightningbot.service
