const fetch =       require('node-fetch');
const SocketIo =    require('socket.io-client');
const Path =        require('path');
const Exec =        require('child_process').exec;

const root = 'https://canary.lightning-tactical.com/';

let _cookies = '';
let _account = null;
let _socketIo = null;
let _madameUrl = null;

async function run()
{
    //console.log('Salut. Bonjour! Ca va?'.split(/(?= [A-Z])/g));

    try
    {
        if((await login('mrgn.development@outlook.fr', 'Pass777')).error)
            console.log('Unable to login this account');
        else
        {
            _account =  await checkSession();

            _operator = await operatorGet();

            initializeSocketIo();

            let conversations = await messagingConversationGet();

            conversations.forEach(conversation =>
                {
                    console.log(conversation.id);
                });

            // Periodic check for a new madame
            setInterval(async () =>
            {
                let url = await bonjourMadameGetUrl();

                if(_madameUrl != url)
                {
                    if(_madameUrl != null)
                    {
                        conversations.forEach(async conversation =>
                            {
                                let quotes = 
                                [
                                    'Alors celle là...', 
                                    'Bonne journée!', 
                                    'Oh ça, c\'est pas pour les bridés!',
                                    'Il est l\'heure!',
                                    'Eh ben... C\'est bon, ils ont mis à jour.'
                                ];

                                await messagingMessageTextSend(conversation.id, quotes[Math.ceil(Math.random() * quotes.length)]);
                                await messagingMessageTextSend(conversation.id, url);
                            });
                    }

                    _madameUrl = url;
                }
            },
            60000);
        }

    }
    catch(error)
    {
        console.error(error);
    }
}

run();


function fetchWithCookies(url, options = {})
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
            // Add the header containing the cookies to the request's options
            options.headers = {accept: '*/*', cookie: _cookies};

            // Regular fetch
            let response = await fetch(url, options);

            // Parse and store cookies for the next requests
            let cookiesRaw = response.headers.raw()['set-cookie'];

            if(cookiesRaw)
                cookiesRaw.forEach(cookieRaw => {
                    _cookies += (_cookies.length ? ';' : '') + cookieRaw.split(';')[0];  
                });

            resolve(response);
        }
        catch(error)
        {
            reject(error);
        }
    });
}

async function login(email, password)
{
    return (await fetchWithCookies(root + 'login.json?email=' + email + '&password=' + password, { method: 'POST' })).json();
}

async function checkSession()
{
    return (await fetchWithCookies(root + 'logged.json', { method: 'GET' })).json();
}

async function operatorGet(id = null)
{
    return (await fetchWithCookies(root + 'operatorGet.json?id=' + id, { method: 'GET' })).json();
}

async function messagingConversationGet()
{
    return (await fetchWithCookies(root + 'messagingConversationsGet', { method: 'GET' })).json();
}

async function messagingMessageTextSend(conversationId, text)
{
    return (await fetchWithCookies(root + 'messagingMessageTextSend?conversationId=' + conversationId + '&text=' + encodeURIComponent(text), {method: 'POST', cache: 'no-cache'})).json();
}

function initializeSocketIo()
{
    _socketIo = SocketIo(root, { extraHeaders: { Cookie: _cookies } });

    _socketIo.on('connect', () =>       { console.log('SocketIO : Connected');     });
    _socketIo.on('disconnect', () =>    { console.log('SocketIO : Disconnected');    });

    _socketIo.on('messaging', async data =>    
    { 
        switch(data.action)
        {
            case 'messageNew':

                // Don't reply to myself
                if(data.message.operatorId != _operator.id)
                {
                    // Potentail response to the received message
                    let response = null;

                    // The message is a text
                    if(data.message.contentType === 'text')
                    {
                        // Ignore case
                        data.message.content = data.message.content.toLowerCase();

                        // Test or debug command
                        if(data.message.content.startsWith('namee'))
                            response = 'Tu es ' + (await operatorGet(data.message.operatorId)).name.toLowerCase() + '!';

                        // Update demand
                        else if(data.message.content.startsWith('update'))
                        {
                            // Master only
                            if(data.message.operatorId == 1)
                            {
                                let updateQuotes = 
                                [
                                    'Et allez, encore des mises à jour...', 
                                    'J\'ai comme l\'impression que ce sera pire après...', 
                                    'Oh merde... Qui a codé ça?',
                                    'Pourquoi ne pas rester sur la vielle image du vieux \'master?\'',
                                    'Si je ne reviends pas ben...',
                                    'Rassurez-moi, le package.json est à jour hein?',
                                    'Les versions... c\'est ma passion...',
                                    'Est-ce-que je vais ENFIN pouvoir donner la météo?',
                                    'Pas besoin, je fonctionne bien (pour le moment).'
                                ];

                                // Immediate message (the app will be restarted during this process)
                                await messagingMessageTextSend( data.message.conversationId, 
                                                                updateQuotes[Math.ceil(Math.random() * updateQuotes.length)]);

                                Exec('/opt/bot/updatewithlog.sh &', async error => 
                                {
                                    if(error)
                                        await messagingMessageTextSend(data.message.conversationId, error);
                                });
                            }
                            else
                                response = 'Non toi, tu ne peux pas faire ça.';
                        }

                        // Respond to my name
                        else if(data.message.content.startsWith(_operator.name.toLowerCase()))
                            response = 'Salut c\'est moi ' + _operator.name + '!'; 
                             
                        // Respond to a madame
                        else if(data.message.content.indexOf('madame') >= 0)
                            response = await bonjourMadameGetUrl();


                        // Random
                        else
                            switch(Math.round(Math.random() * 100))
                            {
                                case 1: response = 'C\'est ça! Il faut terminer le dispositif! Allez-y!'; break;
                                case 2: response = 'Vous vous souvenez de la fois où je ne me rappelais plus ce que j\'avais oublié?'; break;
                                case 3: response = 'Oh ça, c\'est pas clair... Je le sens.'; break;
                                case 4: response = 'On peut repérer ' + (await operatorGet(data.message.operatorId)).name.toLowerCase() + ' à l\'odeur... C\'est pratique.'; break;
                            
                                case 10: response = 'C\'est sale... Mais ça me va.'; break;
                                case 11: response = 'Oh? Je vous ai vu aujourd\'hui les gars? Je suis un peu à la masse ce matin...'; break;
                                case 11: response = 'Vous tombez bien, je cherche le framework pour embeded fedora'; break;
                                
                                case 20: response = 'Psss hey toi... Par ici pss fifiouu!'; break;
                                case 21: response = 'Un homme a dit un jour, la clef du succès, c\est le travail et l\'implication. Ce gars n\a jamais eu de succès.'; break;
                                case 22: response = 'Mais qu\'est-ce-que c\'est qu\'ça?!!'; break;                                                     
                            }

                    }
                    else
                    {
                        // Not a text

                        
                    }

                    // Reply
                    if(response)
                    {
                        let quotes = response.split(/(?= [A-Z])/g);

                        for(var i = 0; i < quotes.length; i++)
                            {
                                await delay(3000);

                                await messagingMessageTextSend(data.message.conversationId, quotes[i]);
                            }
                    }
                }

                console.log(data.message.content);

            break;
        }


        console.log('SocketIO : Event ' + JSON.stringify(data));
    
    });
}

function delay(duration)
{
    return new Promise(resolve =>
        {
            setTimeout(resolve, duration);
        });
}

bonjourMadameGetUrl();

async function bonjourMadameGetUrl()
{
    let page = await(await fetch('http://www.bonjourmadame.fr')).text();
    
    let date  = new Date();
    let year  = date.getFullYear(); 
    let month = (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1);

    let start = /*'https://i1*/'.wp.com/bonjourmadame.fr/wp-content/uploads/' + year + '/' + month + '/';   // The sub domain may change i0, i1...
    let url = '';

    let startIndex = page.indexOf(start);   

    if(startIndex >= 0)
    {
        let endIndex = page.indexOf('?', startIndex);

        url = 'https://' + page.substring(startIndex - 2, endIndex);                                        // -2 for i0, i1...
    }
    
    return url;
}